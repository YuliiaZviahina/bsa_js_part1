//Person by age increase
let person = [{name: "Ann", age: 33}, {name: "Yuliia", age: 31}, {name: "Lida", age: 35}];
person.sort(sortAge);
function sortAge(a, b){
  if(a.age>b.age){
    return 1;
  }
  if(a.age<b.age){
    return -1;
  }
  return 0;
}
console.log(person);

//Person by age decrease
let person = [{name: "Ann", age: 33}, {name: "Yuliia", age: 31}, {name: "Lida", age: 35}];
person.sort(sortAge);
function sortAge(a, b){
  if(a.age<b.age){
    return 1;
  }
  if(a.age>b.age){
    return -1;
  }
  return 0;
}
console.log(person);
