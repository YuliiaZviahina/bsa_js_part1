let person = [{name: "Ann", age: 33}, {name: "Yuliia", age: 31}];
function compareAge() {
    if(person[0].age > person[1].age) {
       console.log(person[0].name + " is older than " + person[1].name);
    }else if(person[0].age < person[1].age) {
        console.log(person[0].name + " is younger than " + person[1].name);
    }else {
        console.log(person[0].name + " is the same age as " + person[1].name);
    }
    
}
compareAge();